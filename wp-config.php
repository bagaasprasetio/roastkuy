<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'roastkuy' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'KVeOG7xRQoDoOeFSMngTkkxC3GQSUNDR5PGZp3NwLHx1baEo8OgKz3SETAZKhyJc' );
define( 'SECURE_AUTH_KEY',  'KpCWLYAYY8f4IQndkLjYY6fZ9d3BBJX53O2D7VHwMDavHOo1hsKq30XkuAtTsRIj' );
define( 'LOGGED_IN_KEY',    'N1LWSqkNu05PvT4PnYLXeDiGcDNWGAfwCdfOWDPTIPbSIvQ2kDMQ0onoJDnk3t6z' );
define( 'NONCE_KEY',        'pw5daxgoY1Yk0SXFwuaB8UVNiwCIW6x3Dn9gTfprHtzwfX5cf6uOGacPnrqZZ7q9' );
define( 'AUTH_SALT',        'COQP3WdZskk0r2imeJ6gNMKaxZgtNrwkuLTxmIGhJTR2GXWEZGwOxP7ZHtfEYt7E' );
define( 'SECURE_AUTH_SALT', 'TLjAHDQ15XtahB6jQM8FLpLf9KtpzcQ5a74PC4kmL3bFWtzkBYQLdcnSvnjUlhyp' );
define( 'LOGGED_IN_SALT',   'JEVBxZEODoHUY97uza7JnLmJIXJKuBbRhcTpZirCQBTAILXb6zQgj848J4wo47Xt' );
define( 'NONCE_SALT',       'DceLbC03i0HPc6zUcmcwgR9WDqxgkpQ7df930qX7WTDEkA8gsQ9IdK76sBYr0n9O' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
